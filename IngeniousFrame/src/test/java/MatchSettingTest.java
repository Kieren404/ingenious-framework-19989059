import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;

public class MatchSettingTest {

	@Test
	public void testMatchSettings() throws IOException, MissingSettingException, IncorrectSettingTypeException {
		MatchSetting m = new MatchSetting("src/test/java/MatchSettingTest.json");
		assertTrue(m.getGameName().equals("TestGame"));
		assertTrue(m.getNumPlayers()==13);
		assertTrue(m.getLobbyName().equals(""));
		Map<String, Object> map = m.getSettingAsMap("testMap");
		assertTrue(map.get("testBool") instanceof Boolean);
		assertTrue(((Boolean)map.get("testBool")) == false);
		assertTrue(map.get("testInt") instanceof Double);
		assertTrue(((Double)map.get("testInt")) == -2);
		assertTrue(map.get("testList") instanceof List<?>);
	}

}
