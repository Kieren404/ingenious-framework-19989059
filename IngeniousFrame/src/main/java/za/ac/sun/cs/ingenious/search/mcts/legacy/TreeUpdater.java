package za.ac.sun.cs.ingenious.search.mcts.legacy;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Describes how the result of a playout should affect the search TreeEngine.
 */
public interface TreeUpdater<N extends SearchNode<? extends GameState,N>> {
	/**
	 * Backpropagate the results of a playout through the search TreeEngine
	 * 
	 * @param node Child node from which to start the backpropagation
	 * @param normalizedResults Normalized (!) map of results per player. That means that all
	 *            results are in [0,1] with 1 indicating a win and 0 indicating a loss.
	 */
	public void backupValue(N node, double[] normalizedResults);
}
