package za.ac.sun.cs.ingenious.games.Risk.network;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

public class RiskInitGameMessage extends InitGameMessage {
    private static final long serialVersionUID = 1L;
    public byte NumPlayers;
    public MatchSetting match;
    private byte playerID;
    public int[] owners;
    /**
     * Constructs a new game initialization message with the player's ID.
     *
     * @param playerID	the players' ID
     */
    public RiskInitGameMessage(byte playerID, byte numPlayers, MatchSetting match, int[] owners)
    {
        this.playerID = playerID;
        this.NumPlayers = numPlayers;
        this.match = match;
        this.owners = owners.clone();
    }

    /**
     * Returns the player's ID.
     *
     * @return	the player's ID
     */
    public byte getPlayerID()
    {
        return playerID;
    }
    public byte getNumPlayers() {
        return NumPlayers;
    }
    public int[] getOwners(){
        return owners;
    }

    public MatchSetting getMatch() {
        return match;
    }

}

