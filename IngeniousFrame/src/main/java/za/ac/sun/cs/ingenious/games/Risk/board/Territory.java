package za.ac.sun.cs.ingenious.games.Risk.board;

import java.util.LinkedList;

public class Territory implements za.ac.sun.cs.ingenious.core.util.state.Node {
    public static int number = 0;
    public String name;
    private int armys=1;
    private int player=0;
    private int continent=0;
    LinkedList<Integer> edges = new LinkedList<Integer>();
    public Territory(String name, int number, int player, int continent) {
        this.name = name;
        this.player = player;
        this.number = number;
        this.continent = continent;
    }
    public void addEdge(int destNode){
        if(!edges.contains(destNode)){
            edges.add(destNode);
        }
    }
    public String getName() {
        return name;
    }
    public int getArmys() {
        return armys;
    }

    public void setArmys(int armys) {
        this.armys = this.armys + armys;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public int getContinent() {
        return continent;
    }

    public void setContinent(int continent) {
        this.continent = continent;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        Territory.number = number;
    }
    public void setEdges(LinkedList<Integer> edge){
        this.edges = edge;
    }
    public LinkedList<Integer> getEdges(){
        return (LinkedList<Integer>) edges.clone();
    }

    public  Territory clone(){
        Territory copy = new Territory(name, number, player, continent);
        copy.setArmys(this.armys);
        copy.setEdges((LinkedList<Integer>) edges.clone());
        return copy;
    }
}
