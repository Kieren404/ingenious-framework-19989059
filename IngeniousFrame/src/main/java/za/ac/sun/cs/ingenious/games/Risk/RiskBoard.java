package za.ac.sun.cs.ingenious.games.Risk;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGraphBoard;
import za.ac.sun.cs.ingenious.games.Risk.board.Territory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class RiskBoard extends TurnBasedGraphBoard {

    public static final int INFANTRY = 0;
    public static final int CAVALRY = 1;
    public static final int ARTILLERY = 2;
    public static final int JOKER = 3;

    public static LinkedList<Territory> board= new LinkedList<Territory>();
    private int numContinents;
    private int numTerritories;
    private int avaliableArmys;
    private int currentPhase = 0;
    private int attack[] = new int[2];

    public void setAttackingTerritory(int aggresor) {
        attack[0] = aggresor;
    }
    public void setDefendingTerritory(int defender) {
        attack[1] = defender;
    }
    public int getAttackingTerritory() {
        return attack[0];
    }
    public int getDefendingTerritory(){
        return attack[1];
    }
    public int getNumAttackers() {
        return numAttackers;
    }

    public void setNumAttackers(int numAttackers) {
        this.numAttackers = numAttackers;
    }

    private int numAttackers=0;
    public int getAttacker() {
        return board.get(attack[0]).getPlayer();
    }

    private int attacker;
    private int[] deck = new int[4];
    private int[] used = new int [4];
    private ArrayList<ArrayList<Integer>> cards = new ArrayList<>();

    public RiskBoard(int numPlayers, int firstPlayer, MatchSetting match, String path) {
        super(firstPlayer, numPlayers);

        Territory newNode;
        for (int i = 0; i < numPlayers;i++) {
            cards.add(i, new ArrayList<>());
        }
        try {
            File map = new File(path);
            Scanner sc = new Scanner(map);
            numContinents = Integer.parseInt(sc.next());
            numTerritories = Integer.parseInt(sc.next());
            for (int i = 0; i < numTerritories; i++) {
                String temp = sc.next();
                newNode = new Territory(temp, i, -1, Integer.parseInt(sc.next()));

                while (sc.hasNextInt()) {
                    newNode.addEdge(Integer.parseInt(sc.next()));
                }
                board.add(newNode);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public RiskBoard(int firstPlayer, int numPlayers){
        super(firstPlayer, numPlayers);
    }
    public void placeArmy(int territory, int number){
        avaliableArmys = avaliableArmys - number;
        board.get(territory).setArmys(number);
    }
    public void moveArmy(int source, int dest, int number){
        board.get(source).setArmys(-1*number);
        board.get(dest).setArmys(number);
    }
    public boolean isOwned(int territory,int player) {
        if(board.get(territory).getPlayer() == player) {
            return true;
        } else {
            return false;
        }
    }
    public void start() {
        //this can be better
        ArrayList<Integer> free = new ArrayList<>();
        int j = 0;
        int picked =0;
        for (int i = 0 ; i < board.size();i++) {
            free.add(i);
        }
        for (int i = 0; i < board.size();i++) {
            picked = (int)(System.currentTimeMillis() % free.size());
            board.get(free.get(picked)).setPlayer(j);
            free.remove(picked);
            if (j == numPlayers-1) {
                j = 0;
            } else {
                j = j + 1;
            }
        }
    }
    public void changeOwner(int territory,int player){
        board.get(territory).setArmys(0);
        board.get(territory).setPlayer(player);
    }
    /**
     * Returns the number (ID) of the player who will play next.
     *
     * @return	the next player's number (ID)
     */
    public int getCurrentPlayer() {
        return nextMovePlayerID;
    }
    public String getTerritoryName(int number) {
        return board.get(number).getName();
    }
    @Override
    public void printPretty() {
        String edges= "";
        Log.info("");
        Log.info("Number of avaliable army's "+ avaliableArmys);
        for(Territory region:board) {
            Log.info("Name: "+region.getName());
            Log.info("Contains "+region.getArmys()+" and is owned by player "+region.getPlayer());
            Log.info("Has access to");
            for(int i : region.getEdges()){
                edges = edges + getTerritoryName(i)+ " ";
            }
            Log.info(edges);
            edges = "";
        }
    }
    public RiskBoard clone(){
        LinkedList<Territory> temp = new LinkedList<Territory>();
        for(Territory region:board) {
            temp.add(region.clone());
        }
        RiskBoard copy = new RiskBoard(0, numPlayers);
        copy.setNumContinents(numContinents);
        copy.setNumTerritorys(numTerritories);
        copy.setBoard(temp);
        return(copy);
    }
    private void setBoard(LinkedList<Territory> board) {
        this.board = board;
    }
    private void setNumTerritorys(int numTerritories) {
        this.numTerritories = numTerritories;
    }

    private void setNumContinents(int numContinents) {
        this.numContinents = numContinents;
    }
    public int[] getOwnerList(){
        int i = 0;
        int[] owners = new int[board.size()];
        for(Territory region:board) {
            owners[i] = region.getPlayer();
            i = i + 1;
        }
        return owners;
    }
    public void setOwnershipList(int[] owners) {
        int i = 0;
        for(Territory region:board) {
            region.setPlayer(owners[i]);
            i = i + 1;
        }
    }
    public int getOwner(int territory) {
        return board.get(territory).getPlayer();
    }
    public int getAvaliableArmys() {
        return avaliableArmys;
    }
    public void setAvaliableArmys() {
        int total = getPlayersTotalArmys(getCurrentPlayer());
            avaliableArmys = total;
            //TODO insert code to increase the number for each continent owend change text file structure add  / 3
    }
    public int getArmysContained(int territory) {
        return board.get(territory).getArmys();
    }
    public boolean isConnected(int source, int dest){
        if (board.get(source).getEdges().contains(dest)) {
            return true;
        } else {
            return false;
        }
    }
    public int getPlayersTotalArmys(int player){
        int total = 0;
        for (Territory region:board) {
            if (region.getPlayer() == player){
                total= total + 1;
            }
        }
        return total;
    }
    public void setCurrentPhase(int currentPhase) {
        this.currentPhase = currentPhase;
    }
    public int getCurrentPhase() {
        return currentPhase;
    }
}
