package za.ac.sun.cs.ingenious.games.Risk;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.games.Risk.network.RiskInitGameMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RiskReferee extends FullyObservableMovesReferee<RiskBoard, RiskLogic, RiskFinalEvaluator> {
    protected int numPlayers;
    protected int firstPlayer;
    protected int numNodes;
    protected MatchSetting match;
    public RiskReferee(MatchSetting match, PlayerRepresentation[] players) throws IncorrectSettingTypeException {
            super(match, players, new RiskBoard(players.length,0,match,"map.txt"), new RiskLogic(),
                    new RiskFinalEvaluator());
            this.match = match;

    }
    //TODO init genaction
    /**
     *
     * @return The init game message that is to be sent to every player before the game starts.
     */
    @Override
    protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
        System.out.println("init game message");
        int owners[] = new int[4];
        owners[0]=0;
        owners[1]=1;
        owners[2]=1;
        owners[3]=0;
        currentState.changeOwner(0,0);
        currentState.changeOwner(1,1);
        currentState.changeOwner(2,1);
        currentState.changeOwner(3,0);
//        return new RiskInitGameMessage((byte) player.getID(),(byte)players.length, match,currentState.getOwnerList());
        return new RiskInitGameMessage((byte) player.getID(),(byte)players.length, match,owners);


    }
    /**
     *
     * @return The genMove message that is to be sent to the current player.
     */
    protected GenActionMessage createGenActionMessage(PlayerRepresentation player) {
        return new GenActionMessage();
    }
    @Override
    protected void beforeGameStarts() {
        currentState.start();
    }

    @Override
    protected void gameLoop() {
        while(!terminated && !logic.isTerminal(currentState)){
            beforeRound();

            Set<Integer> playersToAct = logic.getCurrentPlayersToAct(currentState);
            Map<Integer, PlayActionMessage> messages;
            if (playersToAct.size()==1) {
                int playerToAct = playersToAct.iterator().next();
                messages = new HashMap<Integer, PlayActionMessage>();
                messages.put(playerToAct, players[playerToAct].genAction(createGenActionMessage(players[playerToAct])));
            } else {
                messages = requestActionsParallel(playersToAct);
            }

            List<Integer> actionOrder = resolveActionOrder(messages);
            for (int playerId: actionOrder) {
                Log.trace("Applying action of player " + playerId);
                PlayActionMessage data = messages.get(playerId);
                Action generatedAction = (data == null ? null : data.getAction());
                if (generatedAction == null) {
                    reactToNullAction(playerId, data);
                } else {
                    Log.trace("Player: " + playerId + "(" + players[playerId].toString() + ") sent action: " + generatedAction.toString());
                    if (logic.validMove(currentState, generatedAction) && checkActionAllowedForPlayer(currentState, generatedAction, playerId)) {
                        if (currentState.getCurrentPhase() == RiskAction.DEFEND ) {
                            Log.info("Rolling for defense");
                            data = new PlayActionMessage(logic.roll(currentState, generatedAction));
                        }

                        updateServerState(data);
                        distributeAcceptedAction(data);
                        reactToValidAction(playerId, data);
                    } else {
                        reactToInvalidAction(playerId, data);
                    }
                }
            }

            afterRound(messages);
        }
    }
}
