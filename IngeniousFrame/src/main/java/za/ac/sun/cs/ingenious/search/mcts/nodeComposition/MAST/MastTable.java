package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST;

import za.ac.sun.cs.ingenious.core.Action;

import java.util.concurrent.ConcurrentHashMap;

public class MastTable {

    public ConcurrentHashMap<Action, StoredAction> moveCombinationScores;
    double tau;
    double winToVisitValues = 0;

    /**
     * Constructor to initialise the hashmap and tau
     */
    public MastTable(double tau) {
        this.moveCombinationScores = new ConcurrentHashMap<>(16, (float) 0.75, 16);
        this.tau = tau;
    }

    /**
     * @return the hashmap of move combinations and scores
     */
    public ConcurrentHashMap<Action, StoredAction> getMoveCombinationScores() {
        return moveCombinationScores;
    }

    /**
     * refresh the hashmap to a clean empty hashmap
     */
    public void resetVisitedMoves() {
        moveCombinationScores = new ConcurrentHashMap<>(16, (float) 0.75, 16);
    }

    /**
     * @return the tau value parameter specified in the engine
     */
    public double getTau() {
        return tau;
    }

    /**
     * increments the value used by the MAST algorithm calculated using both the win and visit count values
     * @param val the value by which to increment the field
     */
    public void incWinToVisitValues(double val) {
        winToVisitValues = winToVisitValues + val;
    }

    /**
     * decrement the value used by the MAST algorithm calculated using both the win and visit count values
     * @param val the value by which to decrement the field
     */
    public void decWinToVisitValues(double val) {
        winToVisitValues = winToVisitValues - val;
    }

    /**
     * @return the value used by the MAST algorithm calculated using both the win and visit count values
     */
    public double getWinToVisitValues() {
        return winToVisitValues;
    }

    /**
     * create a new action wrapper which holds information relating to the action which is used by the MAST algorithm
     * @param action
     * @return
     */
    public StoredAction createNewStoredAction(Action action) {
        return new StoredAction(action);
    }

    /**
     * the stored action wrapper for the action
     */
    public class StoredAction {
        private Action action;
        private double visitCount = 0;
        private double wins = 0;

        /**
         * constructor to initialise the action
         * @param action
         */
        public StoredAction(Action action) {
            this.action = action;
        }

        /**
         * increment the number of playouts throughout the game for which this move was played
         */
        public void incVisitCount() {
            visitCount = visitCount + 1.0;
        }

        /**
         * @return the number of playouts throughout the game for which this move was played
         */
        public double getVisitCount() {
            return visitCount;
        }

        /**
         * increment the number of winning playouts throughout the game for which this move was played by 1
         */
        public void incWins() {
            wins = wins + 1.0;
        }

        /**
         * @return the number of winning playouts throughout the game for which this move was played
         */
        public double getWins() {
            return wins;
        }

        /**
         * @return the action for which this wrapper holds values
         */
        public Action getAction() {
            return action;
        }
    }
}
