package za.ac.sun.cs.ingenious.games.Risk.board;

import com.esotericsoftware.minlog.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class RISKGraph implements za.ac.sun.cs.ingenious.core.util.state.Graph {
    //iterator
    public static LinkedList<Territory> graph= new LinkedList<Territory>();
    private int numContinents;
    private int numTerritories;
    public RISKGraph(File map){
        Territory newNode;
        try {
            Scanner sc = new Scanner(map);
            numContinents = Integer.parseInt(sc.next());
            numTerritories = Integer.parseInt(sc.next());
            for (int i = 0; i < numTerritories; i++) {
                String temp = sc.next();
                newNode = new Territory(temp, 0, 0, Integer.parseInt(sc.next()));

                while (sc.hasNextInt()) {
                    newNode.addEdge(Integer.parseInt(sc.next()));
                }
                graph.add(newNode);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void initialOwners(int numPlayers){
        //this can be better
        int[] free = new int[graph.size()];
        int j = 0;
        int picked =0;
        for (int i = 0 ; i < graph.size();i++) {
            free[i] = i;
        }
        for (int i = 0; i < graph.size();i++) {
            picked = (int)(System.currentTimeMillis() % free.length);
            graph.get(free[picked]).setPlayer(j);
            if (j == numPlayers) {
                j = 0;
            } else {
                j = j + 1;
            }
        }
    }
    public void editArmys(int territory, int number){
        graph.get(territory).setArmys(number);
    }

    public void editOwnership(int territory, int player) {
        graph.get(territory).setArmys(0);
        graph.get(territory).setPlayer(player);
    }
    public int getOwner(int territory) {
        return graph.get(territory).getPlayer();
    }
    public String getTerritoryName(int number) {
        return graph.get(number).getName();
    }

    public void print(){
        String edges= "";
        Log.info("");
            for(Territory region:graph) {
                Log.info("Name: "+region.getName());
                Log.info("Contains "+region.getArmys()+" and is owned by player "+region.getPlayer());
                Log.info("Has access to");
                for(int i : region.edges){
                    edges = edges + getTerritoryName(i)+ " ";
                }
                Log.info(edges);
                edges = "";
        }
    }
}
