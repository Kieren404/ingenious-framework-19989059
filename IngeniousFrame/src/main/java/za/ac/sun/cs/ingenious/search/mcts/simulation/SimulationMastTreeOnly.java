package za.ac.sun.cs.ingenious.search.mcts.simulation;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimulationMastTreeOnly<S extends GameState> implements SimulationThreadSafe<S> {

    GameLogic<S> logic;
    MctsGameFinalEvaluator<S> evaluator;
    ActionSensor<S> obs;
    ArrayList<Action> moves = new ArrayList<>();

    private MastTable visitedMovesTable;
    private double tau;
    boolean recordMoves;


    /** constructor used by the game engine which specifies parameters */
    public SimulationMastTreeOnly(GameLogic<S> logic, MctsGameFinalEvaluator<S> evaluator, ActionSensor<S> obs, MastTable visitedMovesTable, double tau, boolean recordMoves) {
        this.logic = logic;
        this.evaluator = evaluator;
        this.obs = obs;
        this.visitedMovesTable = visitedMovesTable;
        this.tau = tau;
        this.recordMoves = recordMoves;
    }

    /**
     * The method that performs random simulation playout.
     *
     * @param state The game state from which to start the simulation playout.
     * @return The result scores
     */
    public SimulationTuple simulate(S state) {
        ArrayList<Action> moveList = new ArrayList<>();
        while (!logic.isTerminal(state)) {
            int playerToPlay = -1;
            for (int playerId : logic.getCurrentPlayersToAct(state)) {
                playerToPlay = playerId;
            }

            List<Action> actions = logic.generateActions(state, playerToPlay);

            Action nextAction;
            nextAction = getNextAction(state, actions);
            if (recordMoves) {
                moves.add(nextAction);
            }
            logic.makeMove(state, obs.fromPointOfView(nextAction, state, playerToPlay));
            moveList.add(nextAction);
        }

        double[] scores = evaluator.getMctsScore(state);
        SimulationTuple returnValues = new SimulationTuple(moves, scores);
        return returnValues;
    }

    /**
     * @param state
     * @return the next action as chosen by the MAST algorithm
     */
    public Action getNextAction(S state, List<Action> possibleActions) {
        Random randomGen = new Random();
        double bestGibbsValueSoFar = 0;

        // if actions are possible, choose one randomly
        if (!possibleActions.isEmpty()) {
            int randomNumber;
            Action actionChoice;

            // stochastic distribution to prevent looping
            if (randomGen.nextInt(100) > 90) {
                randomNumber = randomGen.nextInt(possibleActions.size());
                actionChoice = possibleActions.get(randomNumber);
            } else {
                double denominator;
                MastTable.StoredAction bestAction = null;
                denominator = visitedMovesTable.getWinToVisitValues();
                for (int i = 0; i < possibleActions.size(); i++) {
                    MastTable.StoredAction currentAction = visitedMovesTable.getMoveCombinationScores().get(possibleActions.get(i));


                    double numerator;
                    double currentGibbsValue;

                    if (possibleActions.get(i).getPlayerID() != -1) {
                        if (currentAction != null) {
                            numerator = Math.exp((currentAction.getWins()/currentAction.getVisitCount())/tau);
                            currentGibbsValue = numerator/denominator;
                            if (currentGibbsValue > bestGibbsValueSoFar) {
                                bestGibbsValueSoFar = currentGibbsValue;
                                bestAction = currentAction;
                            }
                        }
                    }
                }

                if (bestAction == null) {
                    randomNumber = randomGen.nextInt(possibleActions.size());
                    actionChoice = possibleActions.get(randomNumber);
                } else {
                    actionChoice = bestAction.getAction();
                }

            }
            return actionChoice;
        }
        Log.error("SimulationRandom", "Error during simulation: state is not terminal, however there is also no actions possible");
        Log.error("SimulationRandom", ("PROBLEM STATE: \n" + state + "\nIs PROBLEM STATE terminal? = " + logic.isTerminal(state)));
        return null;
    }

    /**
     * @return the list of moves made during simulation
     */
    public ArrayList<Action> getMoves() {
        return moves;
    }

    /**
     * Game logic object getter.
     *
     * @return The game logic
     */
    public GameLogic<S> getLogic() {
        return logic;
    }

    public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationMastTreeOnly(GameLogic<SS> logic, MctsGameFinalEvaluator<SS> evaluator, ActionSensor<SS> obs, MastTable visitedMovesTable, double tau, boolean recordMoves) {
        return new SimulationMastTreeOnly(logic, evaluator, obs, visitedMovesTable, tau, recordMoves);
    }

}
