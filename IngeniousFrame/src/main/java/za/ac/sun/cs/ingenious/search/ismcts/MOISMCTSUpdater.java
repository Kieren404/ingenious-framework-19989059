package za.ac.sun.cs.ingenious.search.ismcts;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.mcts.legacy.TreeUpdater;

/**
 * Implementation of the TreeUpdater for use with {@link MOISMCTSNode}. Also adds the reward and visit to
 * the player nodes contained in the MOISMCTSNode.
 * 
 * @author Michael Krause
 */
public class MOISMCTSUpdater<S extends TurnBasedGameState> implements TreeUpdater<MOISMCTSNode<S>> {

	@Override
	public void backupValue(MOISMCTSNode<S> node, double[] normalizedResults) {
		MOISMCTSNode<? extends GameState> iterator = node;
		while (iterator != null) {
			iterator.addReward(normalizedResults);
			iterator.increaseVisits();
			for (ISMCTSNode<? extends GameState> containedNode : iterator.getPlayerNodes()) {
				containedNode.addReward(normalizedResults);
				containedNode.increaseVisits();
			}
			iterator = iterator.getParent();
		}
	}
}
