package za.ac.sun.cs.ingenious.core.util;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Chris Coetzee on 2016/07/28.
 * This was taken from: http://stackoverflow.com/a/21634867/2490686
 * <p/>
 * Some extensions for GSON
 */
public class GsonExtension {
    @Retention(RetentionPolicy.RUNTIME) @Target(value = ElementType.FIELD) public @interface JsonRequired {}

    class AnnotatedDeserializer<T> implements JsonDeserializer<T> {
        public T deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
                throws JsonParseException {
            T pojo = new Gson().fromJson(je, type);

            Field[] fields = pojo.getClass().getDeclaredFields();
            for (Field f : fields) {
                if (f.getAnnotation(JsonRequired.class) != null) {
                    try {
                        f.setAccessible(true);
                        if (f.get(pojo) == null) {
                            throw new JsonParseException("Missing field in JSON: " + f.getName());
                        }
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(AnnotatedDeserializer.class.getName()).log(Level.SEVERE,
                                                                                    null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(AnnotatedDeserializer.class.getName()).log(Level.SEVERE,
                                                                                    null, ex);
                    }
                }
            }
            return pojo;

        }

    }
}
