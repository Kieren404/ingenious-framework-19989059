package za.ac.sun.cs.ingenious.games.cardGames.classicalDeck;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

/**
 * The enum ClassicalSuits.
 */
public enum ClassicalSuits implements CardFeature{

	/** Classical suits. 
	 * 	This order of suits is used in many card games.
	 */
	CLUBS(12),	
	SPADES(11),	
	HEARTS(10),	
	DIAMONDS(9);
			
	/** Value for hierarchical ordering. */	
	int value;
	
	/**
	 * Constructor for suits.
	 *
	 * @param Value of suit.
	 */
	ClassicalSuits(int value){
		this.value = value;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature#getValue()
	 */
	@Override
	public int getValue() {
		return this.value;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature#setValue(int)
	 */
	@Override
	public void setValue(int value) {
	}
}
