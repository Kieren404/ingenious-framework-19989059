package za.ac.sun.cs.ingenious.search.mcts.simulation;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;

import java.util.*;

public class SimulationContextual<S extends GameState> implements SimulationThreadSafe<S> {

    GameLogic<S> logic;
    MctsGameFinalEvaluator<S> evaluator;
    ActionSensor<S> obs;
    ArrayList<Action> moves = new ArrayList<>();
    boolean recordMoves;

    private CMCTable visitedMovesTable;
    double gamma;
    double threshold;

    /**
     * Constructor.
     *
     * @param logic     The game logic used during the simulation.
     * @param evaluator The game evaluator used to determine the results of the simulation.
     * @param obs       Object to process actions made by the players during playouts.
     *                  Actions are observed from the point of view of the environment player.
     * @param visitedMovesTable The object representing all two-move tiles and their corresponding visit counts and win counts.
     */
    public SimulationContextual(GameLogic<S> logic, MctsGameFinalEvaluator<S> evaluator, ActionSensor<S> obs, CMCTable visitedMovesTable, double gamma, double threshold, boolean recordMoves) {
        this.logic = logic;
        this.evaluator = evaluator;
        this.obs = obs;
        this.visitedMovesTable = visitedMovesTable;
        this.gamma = gamma;
        this.threshold = threshold;
        this.recordMoves = recordMoves;
    }

    /**
     * The method that performs a simulation playout based on the CMC enhancement algorithm.
     * @param state The game state from which to start the simulation playout.
     * @return The result scores
     */
    public SimulationTuple simulate(S state) {
        Action[] previousActions = new Action[2];
        previousActions[0] = null;
        previousActions[1] = null;
        ArrayList<Action> moveList = new ArrayList<>();

        while (!logic.isTerminal(state)) {
            int playerToPlay = -1;
            for (int playerId : logic.getCurrentPlayersToAct(state)) {
                playerToPlay = playerId;
            }

            Action nextAction;
            if (playerToPlay == 0) {
                nextAction = getNextAction(playerToPlay, previousActions[0], state);
                previousActions[0] = nextAction;
            } else {
                nextAction = getNextAction(playerToPlay, previousActions[1], state);
                previousActions[1] = nextAction;
            }
            if (recordMoves) {
                moves.add(nextAction);
            }
            logic.makeMove(state, obs.fromPointOfView(nextAction, state, playerToPlay));
            moveList.add(nextAction);
        }
        double[] scores = evaluator.getMctsScore(state);
        if (moveList.size() >= 3) {
            updateVisitedMovesTable(moveList, scores);
        }
        SimulationTuple returnValues = new SimulationTuple(moves, scores);
        return returnValues;
    }

    /**
     * This method updates the global contextual table using the result of the simulation
     * @param moveList
     * @param scores
     */
    public void updateVisitedMovesTable(ArrayList<Action> moveList, double[] scores) {
        for (int i = 0; i < moveList.size()-2; i++) {
            Action firstAction = moveList.get(i);
            Action secondAction = moveList.get(i+2);

            if (firstAction.getPlayerID() != -1 && secondAction.getPlayerID() != -1) {
                double result = scores[firstAction.getPlayerID()];
                if (result == 1) {
                    updateBackpropSimulationPath(firstAction, secondAction, true);
                } else if (result == -1) {
                    updateBackpropSimulationPath(firstAction, secondAction, false);
                }
                // ignore draw state
            }
        }
    }

    /**
     * @param playerToPlay
     * @param previousAction
     * @param state
     * @return the next action as chosen by the Contextual algorithm
     */
    public Action getNextAction(int playerToPlay, Action previousAction, S state) {
        Random randomGen = new Random();
        CMCTable.StoredActions currentStoredActions = null;

        // check for stored replies from CMC table
        if (previousAction != null) {
            currentStoredActions = visitedMovesTable.getMoveCombinationScores().get(previousAction);
        }

        double random = randomGen.nextInt(100);
        if (currentStoredActions != null && random <= gamma*100) {
            double bestScSoFar = Double.MAX_VALUE;
            Action bestAction = null;

            currentStoredActions.setReadLock();
            for (Action a: currentStoredActions.getActions()) {
                if (logic.validMove(state, a)) {
                    double Qcmc = currentStoredActions.getWins(a)/currentStoredActions.getVisitCount(a);
                    double Sc = 0;
                    if (Qcmc >= threshold) {
                        Sc = Qcmc;
                    }
                    if (Sc >= bestScSoFar) {
                        bestScSoFar = Sc;
                        bestAction = a;
                    }
                }
            }
            currentStoredActions.unsetReadLock();

            // If none of the actions are valid for the current board state so get random action
            if (bestAction == null) {
                List<Action> possibleActions = logic.generateActions(state, playerToPlay);

                // if actions are possible, choose one randomly
                if (!possibleActions.isEmpty()) {
                    Action randomAction = possibleActions.get(randomGen.nextInt(possibleActions.size()));
                    return randomAction;
                }
            }
            return bestAction;
        } else {
            List<Action> possibleActions = logic.generateActions(state, playerToPlay);

            // if actions are possible, choose one randomly
            if (!possibleActions.isEmpty()) {
                Action randomAction = possibleActions.get(randomGen.nextInt(possibleActions.size()));
                return randomAction;
            }
        }
        Log.error("SimulationRandom", "Error during simulation: state is not terminal, however there is also no actions possible");
        Log.error("SimulationRandom", ("PROBLEM STATE: \n" + state + "\nIs PROBLEM STATE terminal? = " + logic.isTerminal(state)));
        System.exit(1);
        return null;
    }

    /**
     * backpropagation phase for the simulation phase in which values in the CMC table are updated
     * with simulation move information
     * @param a1 the first tile move
     * @param a2 the second tile move
     * @param win the result of the playout
     */
    public void updateBackpropSimulationPath(Action a1, Action a2, boolean win) {
        CMCTable.StoredActions storedAction =  visitedMovesTable.getMoveCombinationScores().get(a1);

        if (storedAction == null) {
            CMCTable.StoredActions newStoredAction = new CMCTable().createNewStoredAction(a2, win);
            visitedMovesTable.getMoveCombinationScores().put(a1, newStoredAction);
        } else {
            storedAction.setReadLock();
            if (storedAction.actions.contains(a2)) {
                storedAction.unsetReadLock();
                storedAction.setWriteLock();
                storedAction.incVisitCount(a2);
                if (win) {
                    storedAction.incWins(a2);
                }
                storedAction.unsetWriteLock();
            } else {
                storedAction.unsetReadLock();
                storedAction.setWriteLock();
                storedAction.addNewAction(a2);
                storedAction.incVisitCount(a2);
                if (win) {
                    storedAction.incWins(a2);
                }
                storedAction.unsetWriteLock();
            }
        }
    }

    /**
     * @return the list of moves made during the simulation
     */
    public ArrayList<Action> getMoves() {
        return moves;
    }

    /**
     * Game logic object getter.
     * @return The game logic
     */
    public GameLogic<S> getLogic() {
        return logic;
    }

    public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationContextual(GameLogic<SS> logic, MctsGameFinalEvaluator<SS> evaluator, ActionSensor<SS> obs, CMCTable visitedMovesTable, double gamma, double threshold, boolean recordMoves) {
        return new SimulationContextual(logic, evaluator, obs, visitedMovesTable, gamma, threshold, recordMoves);
    }
}

