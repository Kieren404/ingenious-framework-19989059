package za.ac.sun.cs.ingenious.games.Risk;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;

import java.util.List;

public class RiskLogic implements TurnBasedGameLogic<RiskBoard> {
    public static RiskLogic defaultRiskLogic;

    @Override
    public boolean validMove(RiskBoard fromState, Move move) {
        RiskAction changes = (RiskAction) move;
        int moves[] = changes.getMove();
    switch (changes.getMoveType()) {
        case RiskAction.PLAY_CARDS:
            Log.info("asdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
        case RiskAction.PLACE_ARMY:
            Log.info("Error for place army");
            if (fromState.isOwned(moves[0], changes.getPlayer())) {
                if (fromState.getAvaliableArmys() >= moves[1]) {
                    return true;
                }
                Log.info("No avaliable army's for player "+changes.getPlayer());
            }
            Log.info("Territory not owned");
            return false;
        case RiskAction.ATTACK:
            Log.info("Attacking");
            if (fromState.isOwned(moves[0], changes.getPlayer()) && !fromState.isOwned(moves[1], changes.getPlayer())) {
                if (fromState.getArmysContained(moves[0]) > 1) {
                    if (fromState.isConnected(moves[0], moves[1])) {
                        return true;
                    }
                }
                Log.info("Territory not owned");
            }
            return false;

        case RiskAction.MOVE_ARMY:
//            Log.info(moves[0]+" " +moves[1] + " "+ moves[3] + " "+changes.getPlayer());
//            Log.info(fromState.isOwned(moves[0], changes.getPlayer()) + " " +fromState.isOwned(moves[1], changes.getPlayer()));
            if (fromState.isOwned(moves[0], changes.getPlayer()) && fromState.isOwned(moves[1], changes.getPlayer())) {
                if (fromState.getArmysContained(moves[0]) > 1) {
                    if (fromState.isConnected(moves[0], moves[1])) {

                        return true;
                    }
                    Log.info("Territory's not connected");
                }
                Log.info("Not enough army's to move");
            }
            Log.info("Territory not owned");
            return false;
        case RiskAction.END:
            return true;
        case RiskAction.DEFEND:
            Log.info("Defending");
                if (moves[0] >= 1) {
                    return true;
            } else if (moves[0] == -1) {
                    return true;
            }

    }
        return false;
    }

    @Override
    public boolean makeMove(RiskBoard fromState, Move move) {
        int phase;
        RiskAction changes = (RiskAction) move;
        int moves[] = changes.getMove();
        switch (changes.getMoveType()) {
            case RiskAction.PLAY_CARDS:
                Log.info("wut                           --");
                //TODO code to add cards armys
                break;
            case RiskAction.PLACE_ARMY:
                fromState.placeArmy(moves[0], moves[1]);
                break;
            case RiskAction.ATTACK:
                if (moves[0] != -1) {
                    Log.info("Attacking move made");
                    fromState.setNumAttackers(moves[2]);
                    fromState.setAttackingTerritory(changes.getMove()[0]);
                    fromState.setDefendingTerritory(changes.getMove()[1]);
                    fromState.setCurrentPhase(RiskAction.DEFEND);

                }
                break;
            case RiskAction.DEFEND:
                Log.info("Defending move made");
                int defender = fromState.getDefendingTerritory();
                int attacker = fromState.getAttackingTerritory();
                fromState.placeArmy(attacker,changes.getMove()[0] * -1);
                if (fromState.getArmysContained(defender) == changes.getMove()[1]) {
                    fromState.changeOwner(defender, fromState.getOwner(attacker));
                    fromState.placeArmy(defender, 1);
                    fromState.placeArmy(attacker, -1);
                } else {
                    fromState.placeArmy(defender, changes.getMove()[1] *-1);
                }
                if (moves[0] != -1) {
                    fromState.setCurrentPhase(RiskAction.ATTACK);
                }
                break;
            case RiskAction.MOVE_ARMY:
                fromState.moveArmy(moves[0], moves[1], moves[2]);
                break;
            case RiskAction.END:
                //used to move the phase by 1. if move army phase then lets the next player go
                phase = fromState.getCurrentPhase();
                if (phase == RiskAction.MOVE_ARMY) {
                    phase = RiskAction.END;
                } else if(moves[0] == -1) {
                    phase = phase + 1;
                }
                if (fromState.getCurrentPhase() == RiskAction.PLAY_CARDS && moves[1] == 1) {
                    fromState.setAvaliableArmys();
                }
                fromState.setCurrentPhase(phase);
        }
        nextTurn(fromState,move);
        return true;
    }

    @Override
    public void undoMove(RiskBoard fromState, Move move) {
        //same same but differant
    }

    @Override
    public List<Action> generateActions(RiskBoard fromState, int forPlayerID) {
        // depending on what phase the player is in loop through graph running is valid for that type of move

        return null;
    }

    @Override
    public boolean isTerminal(RiskBoard state) {
        //count army on board if only one players army == terminal
            return false;
    }
    public int setDefenderTurn(RiskBoard fromState, Move move) {
        RiskAction changes = (RiskAction) move;
        return fromState.getOwner(changes.getMove()[1]);
    }
//    public boolean firstAttack(Move move) {
//        RiskAction changes = (RiskAction) move;
//
//    }
//    public int RollDefence() {
//
//    }
    //TODO probably
    public void nextTurn(RiskBoard fromState, Move move) {
        //TODO add ability to set next player when able to defend
        RiskAction changes = (RiskAction) move;
        if (changes.getMoveType() == RiskAction.ATTACK) {
            fromState.nextMovePlayerID = setDefenderTurn(fromState, move);
        }
        if(changes.getMoveType() == RiskAction.DEFEND) {
            fromState.nextMovePlayerID = fromState.getAttacker();
        }
        if (changes.getMoveType() == RiskAction.END && fromState.getCurrentPhase() == RiskAction.END) {
            fromState.setCurrentPhase(RiskAction.PLAY_CARDS);
            fromState.nextMovePlayerID = (fromState.nextMovePlayerID + 1) % fromState.getNumPlayers();
        }
    }
    public Action roll(RiskBoard fromState, Action move){
        RiskAction changes = (RiskAction) move;
        int attack = fromState.getNumAttackers();
        int defend = changes.getMove()[0];
        int[] attackersRolls = new int[3];
        int[] defendersRolls = new int[2];
        int[] results = new int[4];
        int most;
        results[2] = changes.getMove()[0];
        results[3] = changes.getMove()[1];
        //roll
        for (int i = 0; i < attack;i++) {
            attackersRolls[i] = (int) (Math.random( )*6 + 1);
            Log.info(attackersRolls[i]);
        }
        for (int i = 0; i < defend;i++) {
            defendersRolls[i] = (int) (Math.random( )*6 + 1);
            Log.info(defendersRolls[i]);
        }

        if(defend < attack ) {
            most = defend;
        } else {
            most = attack;
        }
        attack = 0;
        defend = 0;
        for (int i = 0;i < most;i++) {
            if (attackersRolls[i] > defendersRolls[i]) {
                results[1] = results[1] + 1;
            } else {
                results[0] = results[0] + 1;
            }
        }
        Log.info("Rolling outcome");
        Log.info("Attacker lost "+results[0]+" units");
        Log.info("Defender lost "+results[1]+" units");
        return move = new RiskAction(changes.getPlayer(), results, changes.getMoveType());
    }
}
