package za.ac.sun.cs.ingenious.games.Risk;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.*;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.Risk.network.RiskInitGameMessage;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class RiskEngine extends Engine {
    protected RiskBoard board;
    protected RiskLogic logic = new RiskLogic();
    protected RiskFinalEvaluator evaluator = new RiskFinalEvaluator();

     /**
     * @param toServer An established connection to the GameServer
     */
     //TODO
    public RiskEngine(EngineToServerConnection toServer, String a, int b) {
        super(toServer);
        board = null;
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {

    }

    @Override
    public String engineName() {
        return null;
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {

    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        RiskInitGameMessage message = (RiskInitGameMessage) a;
        this.playerID =message.getPlayerID();
        File map = new File("map.txt");
        //TODO use json to set num players + map when setting up board
        board = new RiskBoard(message.getNumPlayers(), 0, message.getMatch(),"map.txt");
        board.setOwnershipList(message.getOwners());
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        RiskAction move = (RiskAction) a.getMove();
        logic.makeMove(board, move);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        RiskAction action;
        int move[] = new int[30];
        String input = "";
        String[]split = new String[0];
        if(board.getCurrentPhase() == RiskAction.PLACE_ARMY){
          //  board.setAvaliableArmys();
        }
        board.printPretty();
        //TODO Generate and display the valid moves that may be picked
        //TODO add in validation and repeats
        try {
            switch (board.getCurrentPhase()) {
                case RiskAction.PLAY_CARDS:
                    input = promptInput("If you wish to play cards select option");
                    split = input.split(" ");
                    move[0] = Integer.parseInt(split[0]);
                    break;
                case RiskAction.PLACE_ARMY:
                    input = promptInput("Select the Territory and number of armys you wish to place");
                    split = input.split(" ");
                        move[0] = Integer.parseInt(split[0]);
                    if(Integer.parseInt(split[0]) != -1) {
                        move[1] = Integer.parseInt(split[1]);
                    }
                    break;
                case RiskAction.ATTACK:
                    input = promptInput("Select Source and Destinantion Territorys and the number of armys to attack with");
                    split = input.split(" ");
                    move[0] = Integer.parseInt(split[0]);
                    if(Integer.parseInt(split[0]) != -1) {
                        move[1] = Integer.parseInt(split[1]);
                        move[2] = Integer.parseInt(split[2]);
                    }
                    break;
                case RiskAction.DEFEND:
                    input = promptInput("Select the number of armys to defend with");
                    split = input.split(" ");
                    move[0] = Integer.parseInt(split[0]);
                    break;
                case RiskAction.MOVE_ARMY:
                    input = promptInput("Select the soyrce and destination Territorys and the number of armys to move");
                    split = input.split(" ");
                    move[0] = Integer.parseInt(split[0]);
                    if(Integer.parseInt(split[0]) != -1) {
                        move[1] = Integer.parseInt(split[1]);
                        move[2] = Integer.parseInt(split[2]);
                    }
                    break;
                case RiskAction.END:
                    Log.info("HMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
                    break;
            }
        }catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            if (move[0] == -1 && RiskAction.PLAY_CARDS == board.getCurrentPhase()) {
                board.setAvaliableArmys();
                //cards here
                move[1] = 1;
                action = new RiskAction(playerID, move, RiskAction.END);
            }else if (move[0] == -1) {
                action = new RiskAction(playerID, move, RiskAction.END);
            }else if (RiskAction.ATTACK == board.getCurrentPhase()) {
                action = new RiskAction(playerID, move, RiskAction.ATTACK);
            }else if (RiskAction.DEFEND == board.getCurrentPhase()) {
                action = new RiskAction(playerID, move, RiskAction.DEFEND);
            } else {
                action = new RiskAction(playerID, move, board.getCurrentPhase());
            }

        return new PlayActionMessage(action);
    }
//from othello
    private String promptInput(String promptMessage) throws IOException, InterruptedException
    {
        String input = "";

        /* TODO: TUI things */

        Log.info(promptMessage);

        /* very verbose, but safe, read from system.in */
        synchronized (System.in) {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            while (!in.ready()) {
                synchronized (this) {
                    wait(10);
                }
            }

            if (in.ready()) {
                input = in.readLine();
            }
        }

        return input;
    }
}

