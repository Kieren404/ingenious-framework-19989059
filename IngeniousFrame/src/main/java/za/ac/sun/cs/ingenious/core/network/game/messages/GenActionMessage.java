package za.ac.sun.cs.ingenious.core.network.game.messages;

import za.ac.sun.cs.ingenious.core.network.Message;
import za.ac.sun.cs.ingenious.core.time.Clock;


/**
 * Message used by the server to indicate to a player that they are required to generate
 * an action. The player replies with a {@link PlayActionMessage}.
 * 
 * @author Stephan Tietz
 */
public class GenActionMessage extends Message {

	private static final long serialVersionUID = 1L;
	
	private Clock clock;
	
	/**
	 * Creates a GenActionMessage that gives the player unlimited time to create their action
	 */
	public GenActionMessage() {
		this(new Clock(-1));
	}
	
	/**
	 * @param clock Provides information about how much time the player has to generate
	 *            their action
	 */
	public GenActionMessage(Clock clock) {
		this.clock = clock;
	}
	
	/**
	 * @return A Clock that contains the milliseconds that are left to make this move.
	 */
	public Clock getClock() {
		return clock;
	}

}
