package za.ac.sun.cs.ingenious.core.util.state;

import java.util.HashMap;
import java.util.LinkedList;

public interface Node {
    LinkedList<Integer> edges = new LinkedList<Integer>();
    //Object stored in list in the graph object
    //each node has a linked list of nodes it is connected too and any other data that needs to be stored in the node
    public default void addEdge(int destNode){
    }

}
