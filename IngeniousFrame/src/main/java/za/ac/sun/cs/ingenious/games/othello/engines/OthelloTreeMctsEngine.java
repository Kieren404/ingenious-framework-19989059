package za.ac.sun.cs.ingenious.games.othello.engines;

// TODO: check which is needed in the end/clean up!!
import java.util.ArrayList;
import java.util.Hashtable;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloMctsFinalEvaluator;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.search.mcts.MctsProcess.MctsTree;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;

import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.selection.FinalSelectionUct.newFinalSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUct.newTreeSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;


// specific othello
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloBoard;

/**
 * An engine for the Othello (Reversi) board game which plays random moves. 
 * The engine represents a player and receives game updates and requests
 * from the Referee (server).
 */
public class OthelloTreeMctsEngine extends OthelloEngine {

	MctsTree<OthelloBoard, MctsNodeTreeParallel<OthelloBoard>> mcts;
	ArrayList<BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>>> backpropagationEnhancements = new ArrayList<>();
	Hashtable<String, MctsNodeExtensionParallelInterface> enhancementExtensionClasses = new Hashtable<>();

	protected int TURN_LENGTH = 3000;
	protected int THREAD_COUNT = 4;

	/**
	 * Constructs a new engine.
	 *
	 * @param serverConnection	the connection the engine uses to receive and send
	 *							information from and to the server
	 */
	public OthelloTreeMctsEngine(EngineToServerConnection serverConnection, String enhancementConfig, int threadCount)
	{
		super(serverConnection);

		double cValue = 0.139;

		TreeSelection<MctsNodeTreeParallel<OthelloBoard>> selection = newTreeSelectionUct(logic, cValue, playerID);

		TreeSelectionFinal<MctsNodeTreeParallel<OthelloBoard>> finalSelection = newFinalSelectionUct(logic);

		ExpansionThreadSafe<MctsNodeTreeParallel<OthelloBoard>, MctsNodeTreeParallel<OthelloBoard>> expansion = newExpansionSingle(logic);
			
		SimulationThreadSafe<OthelloBoard> simulation =
			newSimulationRandom(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), false);
		
		BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backprop = newBackpropagationAverage();
		backpropagationEnhancements.add(backprop);

		this.mcts = new MctsTree<>(selection, expansion, simulation, backpropagationEnhancements, finalSelection, logic, THREAD_COUNT, playerID);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	/**
	 * Returns the Engine's name
	 *
	 * @return	the engine's name
	 */
	public String engineName()
	{
		return "OthelloTreeMctsEngine";
	}

	/**
	 * Requests a move from the player and determines if the move is valid.
	 * If the move is valid, it is sent to the Referee. Otherwise the player
	 * is asked to generate a new move until the player's clock runs out or
	 * the maximum wrong moves is reached.
	 *
	 * @param message	the generate move request sent from the Referee
	 *
	 * @return			a valid move, or null if the clock ran out, the
	 *					player's tries ran out, or the player has forfeit
	 */
	public PlayActionMessage receiveGenActionMessage(GenActionMessage message) {
		if (output) {
			board.printPretty();
		}

		Hashtable<String, MctsNodeExtensionParallelInterface> newEnhancementExtensionClasses = new Hashtable<>();
		for (MctsNodeExtensionParallelInterface newExtension: enhancementExtensionClasses.values()) {
			try {
				newEnhancementExtensionClasses.put(newExtension.getID(), newExtension.getClass().newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		MctsNodeTreeParallel<OthelloBoard> root = new MctsNodeTreeParallel<OthelloBoard>(board, null, null, new ArrayList<MctsNodeTreeParallel<OthelloBoard>>(), logic, newEnhancementExtensionClasses, playerID);

		Action action = mcts.doSearch(root, TURN_LENGTH, null).getPrevAction();
		if (action == null) {
			return new PlayActionMessage(new ForfeitAction((byte) playerID));
		}

		return new PlayActionMessage(action);
	}
}

// TODO: comments!! CORRECT!!! and add!!! Current comments are INCORRECT!!!
// TODO: play with sending classes and having factory methods perhaps? make it more user friendly (new policies made things complicated)
