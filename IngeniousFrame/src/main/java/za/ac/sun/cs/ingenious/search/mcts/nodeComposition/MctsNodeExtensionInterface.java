package za.ac.sun.cs.ingenious.search.mcts.nodeComposition;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;

public interface MctsNodeExtensionInterface {

    String getID();

   void setUp(MctsNodeComposition node);

}