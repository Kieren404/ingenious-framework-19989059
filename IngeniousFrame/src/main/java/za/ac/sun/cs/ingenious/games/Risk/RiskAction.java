package za.ac.sun.cs.ingenious.games.Risk;

import za.ac.sun.cs.ingenious.core.Action;

public class RiskAction implements Action {
    public static final int PLAY_CARDS = 0;
    public static final int PLACE_ARMY = 1;
    public static final int ATTACK = 2;
    public static final int MOVE_ARMY= 3;
    public static final int DEFEND = 4;
    public static final int END = 5;
    //cahnges to board
    //placed armys
    //attacks
    //movement
    /**
     * Layout of a move array
     * place army
     * Destination Territory, number of armys
     * Attack
     * Source Territory, Destination Territory, Number of armys to Commit
     * Defend
     *
     * Move Army
     * Source Territory, Destination Territory, Number of army's to commit
     *
     * end Turn
     * no values
     * this is used to let the referee know it can move on to the next player
     *
     * Defend
     * Number of armys to defend with
     * Current Phase
     */
    private int player;
    private int move[];
    private int moveType;
    private int armys;
    public RiskAction(int player, int move[], int moveType){
        this.player = player;
        this.move = move;
        this.moveType = moveType;
    }
    public int getPlayer() {
        return player;
    }
    public int[] getMove() {
        return move;
    }
    public int getMoveType() {
        return moveType;
    }
    @Override
    public int getPlayerID() {
        return player;
    }
    public int getArmys(){
        return armys;
    }
    public void setArmys(int armys) {
        this.armys = armys;
    }
    public void setMoveType(int type) {
        this.moveType = type;
    }
}
