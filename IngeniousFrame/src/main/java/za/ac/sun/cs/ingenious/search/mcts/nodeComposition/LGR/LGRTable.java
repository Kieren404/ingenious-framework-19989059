package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR;

import za.ac.sun.cs.ingenious.core.Action;

import java.util.concurrent.ConcurrentHashMap;

public class LGRTable {

    public ConcurrentHashMap<Action, Action> moveCombinationScores;

    /**
     * Constructor to initialise the hashmap
     */
    public LGRTable() {
        this.moveCombinationScores = new ConcurrentHashMap<>();
    }

    /**
     * refresh the hashmap to a clean empty hashmap
     */
    public void resetVisitedMoves() {
        moveCombinationScores = new ConcurrentHashMap<>();
    }

    /**
     * @return the hashmap of move combinations and scores
     */
    public ConcurrentHashMap<Action, Action> getMoveCombinationScores() {
        return moveCombinationScores;
    }

}
