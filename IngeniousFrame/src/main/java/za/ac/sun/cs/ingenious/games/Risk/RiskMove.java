package za.ac.sun.cs.ingenious.games.Risk;

import za.ac.sun.cs.ingenious.core.Move;

public class RiskMove implements Move {
//    public static final int PLACE_ARMY = 0;
//    public static final int ATTACK = 1;
//    public static final int MOVE_ARMY= 2;
    //cahnges to board
    //placed armys
    //attacks
    //movement
    private int player;
    private int placed[];
    private int attacks[];
    private int moves[];
    public RiskMove(int player, int placed[], int attacks[], int moves[]){
        this.player = player;
        this.placed = placed;
        this.attacks = attacks;
        this.moves = moves;
    }
    public int getPlayer() {
        return player;
    }
    public int[] getPlaced() {
        return placed;
    }
    public int[] getAttacks() {
        return attacks;
    }
    public int[] getMoves() {
        return moves;
    }


    //referance for node from
    // ref for node to
    //num armys
    //player

    @Override
    public int getPlayerID() {
        return 0;
    }
}
